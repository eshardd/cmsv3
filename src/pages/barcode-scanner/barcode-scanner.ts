import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Api } from '../../services/api';
import { Storage } from '@ionic/storage';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, CameraPosition, MarkerOptions, Marker, Environment} from '@ionic-native/google-maps';
import { SettingsPage } from "../settings/settings";

@IonicPage()
@Component({
	selector: 'page-barcode-scanner',
	templateUrl: 'barcode-scanner.html',
})
export class BarcodeScannerPage implements OnInit {

	barcodeForm : FormGroup;
	map: GoogleMap;
	barcode: string;
	longitude: number;
	latitude: number;
	showdetail:boolean = false;

	constructor(
		public navCtrl: NavController,
		public nav: NavController,
		public navParams: NavParams,
		private barcodeScanner: BarcodeScanner,
		private formBuilder: FormBuilder,
		public api: Api,
		private storage: Storage,
		private geolocation: Geolocation
	) {
		this.barcodeForm = this.formBuilder.group({
			num:    ['', Validators.required]
		});
	}

	ngOnInit(){
		//this.loadMap();
	}

	goToAccount() {
		this.nav.push(SettingsPage);
	}

	watchlocation(barcode:string){
		let watch = this.geolocation.watchPosition();
		watch.subscribe((data) => {
			this.barcode = barcode;
			this.longitude = data.coords.longitude;
			this.latitude = data.coords.latitude;
			this.showdetail = true;
			this.loadMap(barcode,data.coords.latitude,data.coords.longitude);
		});
	}

	submitForm(param:string){
		this.barcodeForm.get('num').setValue(param);
		this.watchlocation(param);
	}

	submitForm1(){
		console.log(this.barcodeForm.value.num);
	}

	scan() {
		this.barcodeScanner.scan().then(data => {
			this.submitForm(data.text);
		});
	}

	loadMap(barcode,latitude,longitude) {

		let mapOptions: GoogleMapOptions = {
	  		camera: {
				target: {
					lat: latitude,
					lng: longitude
				},
			zoom: 18,
			tilt: 30
			}
		};

		this.map = GoogleMaps.create('map_canvas', mapOptions);

		this.map.addMarker({
			title: 'Tray no "'+barcode+'" Location ',
			icon: 'red',
			animation: 'DROP',
			position: {
				lat: latitude,
				lng: longitude
			}
		}).then((marker:Marker)=>{
			marker.showInfoWindow();
		});
	}
}
