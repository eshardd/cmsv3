import { Component, OnInit} from "@angular/core";
import { IonicPage, NavController, ToastController, AlertController, LoadingController,MenuController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegisterPage } from "../register/register";
import { BarcodeScannerPage } from "../barcode-scanner/barcode-scanner";
import { Api } from '../../services/api';
//import { Storage } from '@ionic/storage';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})
export class LoginPage implements OnInit {

	loginForm : FormGroup;

	constructor(
		public nav: NavController, 
		public forgotCtrl: AlertController, 
		public menu: MenuController, 
		public toastCtrl: ToastController,
		public api: Api,
		//private storage: Storage,
		private formBuilder: FormBuilder,
		public alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		public navCtrl: NavController,) {
		this.menu.swipeEnable(true);
	}

	ngOnInit(){
		this.loginForm = this.formBuilder.group({
			 email:     ['', Validators.email],
			 password:   ['', Validators.required]
		 });
	}

	// go to register page
	register() {
		this.nav.setRoot(RegisterPage);
	}

	logForm() {

		/*let loader = this.loadingCtrl.create({
			 content: "Please wait..."
		 });

		this.api.postData('authenticate/login',this.loginForm.value).subscribe((data: any)=> {
				if (data.user != null) {
					this.storage.set('currentUser', data.user);
					let toast = this.toastCtrl.create({
						message: "Success",
						duration: 3000,
						position: 'top'
					});
					toast.present();
					this.nav.setRoot(BarcodeScannerPage);
				}else{
					let toast = this.toastCtrl.create({
					 message: "Login Failed",
					 duration: 3000,
					 position: 'top'
				 });
				 toast.present();
				 this.loginForm.reset();
				}
				loader.dismiss();
			});*/
		this.nav.setRoot(BarcodeScannerPage);
	}

	forgotPass() {
		let forgot = this.forgotCtrl.create({
			title: 'Forgot Password?',
			message: "Enter you email address to send a reset link password.",
			inputs: [
				{
					name: 'email',
					placeholder: 'Email',
					type: 'email'
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: data => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Send',
					handler: data => {
						console.log('Send clicked');
						let toast = this.toastCtrl.create({
							message: 'Email was sended successfully',
							duration: 3000,
							position: 'top',
							cssClass: 'dark-trans',
							closeButtonText: 'OK',
							showCloseButton: true
						});
						toast.present();
					}
				}
			]
		});
		forgot.present();
	}

}
