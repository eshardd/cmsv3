import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Response, Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { Global } from './global';


/**
 * Api is a generic REST Api handler. Set your API url first.
 */
 @Injectable()
 export class Api {
   url: string = 'http://localhost/e-netciti-api/public/';

   private header: any;

   constructor(private http: Http, private global: Global) 
   { 

     let temp = new Headers({
       'Content-Type': 'application/json'
       //'Authorization': 'Bearer '+token
     });
     this.header = new RequestOptions({ headers: temp });

   }

   public getData(table: string): Observable<object[]> {
     return this.http.get(this.global._urlApi+table, this.header).pipe(map((res: Response) => res.json()));
   }

   public updateData(table: string, body: object): Observable<object[]> {
     return this.http.put(this.global._urlApi+table, body, this.header).pipe(map((res: Response) => res.json()));
   }

   public postData(table: string, body: object): Observable<object[]> {
     return this.http.post(this.global._urlApi+table, body, this.header).pipe(map((res: Response) => res.json()));
   }

 }
